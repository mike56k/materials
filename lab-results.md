## Результаты автоматической проверки результатов выполнения ДЗ.

Описание основных проблем.

|проблема|причина|рекомендация|
|-----|-------|----|
|меня нет в таблице|староста не внес информацию о вашем хранилище в таблицу|завести хранилище и передать старосте|
|все столбцы про лабы пусты|структура хранилища не соотвествует правилам, неправильно указана ссылка на хранилище|привести в соотвествие с требованиями|
|лаба - build-failed|нет библиотеки или интерфейс типа отличается от эталона|привести в соотвествие с примером и требованиями|
|лаба - skiped|папка библиотеки в дереве не найдена|реализовать библиотеку|
|у меня нет примечания|ничего не значит, могли не дойти руки|приводить в порядок остальные пункты, пока все лабы не будут test-ok|

### Обратите внимание
Код лабораторных живет в папке prj.lab.

Проверка кода на первом этапе выполняется автоматически, поэтому в части интерфейса надо ориентироваться на примеры в моем хранилище.

В частности, для типов матриц:
1. разрешаем размер (0,0) для умолчательно созданных
2. запрещаем нулевой размер по одному из измерений
3. обращаем внимание на порядок аргументов в конструкторе
```
Matrix(const std::ptrdiff_t col_count, const std::ptrdiff_t row_count)
```

### Последние результаты

|group|student|user|complex|rational|matrixds|matrixls|matrixvs|matrixdr|matrixlr|matrixvr|stackl|queuea|notes|
|-----|-------|----|------|------|------|------|------|------|------|------|------|------|-----|
|bpm-19-1|Абдуллин Амир|abdullin_a_a|build-failed|build-failed|skipped|skipped|build-failed|skipped|skipped|build-failed|skipped|skipped||
|bpm-19-1|Альмиева Ралина|almieva_r_r|test-ok|test-failed|test-ok|test-ok|test-ok|test-ok|test-failed|test-ok|skipped|skipped||
|bpm-19-1|Балаев Антон|balaev_a_a|build-failed|build-failed|skipped|skipped|build-failed|skipped|skipped|skipped|skipped|skipped||
|bpm-19-1|Бекетов Роман|beketov_r_i|build-failed|build-failed|skipped|skipped|build-failed|skipped|skipped|skipped|skipped|skipped||
|bpm-19-1|Воеводина Виктория|voevodina_v_e|build-failed|build-failed|build-failed|build-failed|build-failed|build-failed|skipped|skipped|skipped|skipped||
|bpm-19-1|Волков Сергей|volkov_s||
|bpm-19-1|Дин Дмитрий|din_d||
|bpm-19-1|Дьяков Владислав|dyakov_v_d|build-failed|test-ok|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-1|Ефремов Даниил|efremov_d_m|build-failed|build-failed|build-failed|build-failed|skipped|build-failed|skipped|skipped|skipped|skipped||
|bpm-19-1|Козина Анастасия|kozina_a_e|test-ok|test-failed|test-ok|test-ok|test-ok|skipped|skipped|test-failed|skipped|skipped||
|bpm-19-1|Колесникова Дарья|kolesnikova_d_p|build-failed|build-failed|skipped|skipped|build-failed|skipped|skipped|skipped|skipped|skipped||
|bpm-19-1|Кувшинов Марк|kuvshinov_m_e|build-failed|build-failed|build-failed|build-failed|test-failed|build-failed|build-failed|test-failed|skipped|skipped||
|bpm-19-1|Личко Дмитрий|lichko_d_a|test-failed|test-failed|test-ok|test-ok|test-ok|test-ok|test-failed|test-failed|skipped|skipped||
|bpm-19-1|Марданшин Камиль|mardanshin_k_i|test-ok|test-failed|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-failed|test-failed||
|bpm-19-1|Матяш Дмитрий|matyash_d_s|build-failed|build-failed|build-failed|build-failed|build-failed|build-failed|build-failed|build-failed|build-failed|build-failed||
|bpm-19-1|Мисютин Вячеслав|misutin_v_a|build-failed|test-failed|test-ok|skipped|test-ok|skipped|skipped|skipped|skipped|skipped||
|bpm-19-1|Мырзаханов Михаил|myrzakhanov_m_a|test-ok|test-failed|test-failed|test-ok|test-failed|build-failed|build-failed|build-failed|skipped|skipped||
|bpm-19-1|Пискевич Дариуш|piskevich_d||
|bpm-19-1|Полетаев Дмитрий|poletaev_d_i|test-ok|test-failed|skipped|skipped|build-failed|skipped|skipped|skipped|skipped|skipped||
|bpm-19-1|Попов Дмитрий|popov_d||
|bpm-19-1|Пырко Алексей|pyrko_a_v|test-ok|build-failed|test-ok|test-ok|test-ok|skipped|skipped|skipped|skipped|skipped||
|bpm-19-1|Радыгин Никита|radygin_n_s|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-1|Рамзайцев Даниил|ramzaicev_d_a|test-ok|test-failed|test-failed|test-failed|test-failed|test-failed|test-failed|test-failed|skipped|skipped||
|bpm-19-1|Рукосуев Валерий|rukosuev_v||
|bpm-19-1|Сергеева Дарья|sergeeva_d_v|build-failed|build-failed|skipped|skipped|build-failed|skipped|skipped|skipped|skipped|skipped||
|bpm-19-1|Сидулин Данила|sidulin_d_a|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-1|Синицын Богдан|sinicyn_b||
|bpm-19-1|Тищенко Анастасия|tischenko_a|build-failed|test-ok|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-1|Федоров Илья|fedorov_i_a||
|bpm-19-1|Фоменко Елена|fomenko_e_e|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||

|group|student|user|complex|rational|matrixds|matrixls|matrixvs|matrixdr|matrixlr|matrixvr|stackl|queuea|notes|
|-----|-------|----|------|------|------|------|------|------|------|------|------|------|-----|
|bpm-19-2|Акимова Алина|akimova_a_a|build-failed|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-2|Антонов Илья|antonov_i_a|build-failed|build-failed|build-failed|build-failed|build-failed|build-failed|skipped|skipped|test-failed|test-failed||
|bpm-19-2|Багильдинская Мария|bagildinskaia_m_s|build-failed|build-failed|skipped|skipped|build-failed|skipped|skipped|skipped|skipped|skipped||
|bpm-19-2|Богатов Дмитрий|bogatov_d_s|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-2|Богданов Артём|bogdanov_a_a|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok||
|bpm-19-2|Богомолов Владислав|bogomolov_v|build-failed|test-failed|build-failed|build-failed|build-failed|skipped|skipped|skipped|skipped|skipped||
|bpm-19-2|Борисова Софья|borisova_s_r|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-2|Гаврилова Анастасия|gavrilova_a_y|build-failed|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-2|Григорьева Алина|grigoryeva_a_r|build-failed|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-2|Дубинский Даниил|dubinsky_d_d|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-2|Епифанов Кирилл|epifanov_k_a|test-ok|test-failed|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|build-failed|test-failed||
|bpm-19-2|Ивершин Вадим|ivershin_v_s|test-ok|test-failed|test-ok|test-ok|test-ok|test-failed|test-ok|test-failed|test-failed|build-failed||
|bpm-19-2|Исаченко Михаил|isachenko_m_k|test-ok|test-failed|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-failed||
|bpm-19-2|Ишмухамедов Артём|ishmuhamedov_a||
|bpm-19-2|Карнаушко Владимир|karnaushko_V|test-ok|test-failed|test-ok|test-ok|test-failed|test-ok|test-failed|test-ok|test-failed|test-failed||
|bpm-19-2|Комлев Данила|komlev_d_a|test-ok|build-failed|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-failed|test-ok||
|bpm-19-2|Котелевский Артем|kotelevsky_a_a|test-ok|build-failed|build-failed|build-failed|build-failed|skipped|skipped|skipped|skipped|skipped||
|bpm-19-2|Круглов Артём|kruglov_a_m||
|bpm-19-2|Мелёхин Никита|melehin_n_i|build-failed|build-failed|test-ok|test-ok|build-failed|test-failed|test-failed|test-failed|skipped|skipped||
|bpm-19-2|Панин Григорий|panin_g_i|build-failed|build-failed|test-failed|test-failed|test-failed|test-failed|build-failed|test-failed|skipped|skipped||
|bpm-19-2|Парчиев Рауф|parchiev_r_b|test-ok|test-failed|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-failed||
|bpm-19-2|Пачков Вячеслав|pachkov_v_v||
|bpm-19-2|Погонин Сергей|pogonin_s_a|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-2|Ханевский Артем|khanevsky_a_g||
|bpm-19-2|Цинпаев Умар|tcinpaev_u_s|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-2|Черных Александр|chernykh_a||
|bpm-19-2|Чуприна Мария|chuprina_m_s|test-ok|build-failed|test-ok|test-ok|test-ok|test-failed|test-ok|test-ok|test-failed|test-failed||

|group|student|user|complex|rational|matrixds|matrixls|matrixvs|matrixdr|matrixlr|matrixvr|stackl|queuea|notes|
|-----|-------|----|------|------|------|------|------|------|------|------|------|------|-----|
|bpm-19-3|Абрамов Денис Кириллович|abramov_d_k|build-failed|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-3|Астраханцев Никита Павлович|astrahancev_n_p||
|bpm-19-3|Гаврин Илья Антонович|gavrin_i_a|build-failed|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-3|Данкин Данила Владимирович|dankin_d_v||
|bpm-19-3|Золотухин Федор Владимирович|zolotuhin_f_v||
|bpm-19-3|Измайлов Лев Сергеевич|izmaylov_l_s|build-failed|build-failed|build-failed|skipped|build-failed|skipped|skipped|skipped|skipped|skipped||
|bpm-19-3|Камилов Айдунбек Саидов|kamilov_a_s||
|bpm-19-3|Ким Александр Станиславович|kim_a_s||
|bpm-19-3|Клейменов Владимир Евгеньевич|kleymenov_v_e|test-failed|test-failed|skipped|skipped|build-failed|skipped|skipped|build-failed|skipped|skipped||
|bpm-19-3|Кокаев Глеб Константинович|kokaev_g_k||
|bpm-19-3|Конев Тимофей Владимирович|konev_t_v|build-failed|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-3|Кузнецов Денис Андреевич|kuznetsov_d_a|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-3|Курлов Игорь Алексеевич|kurlov_i_a|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-3|Максаков Владимир Александрович|maksakov_v_a|test-failed|test-failed|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-3|Машуров Владимир Владимирович|mashurov_v_v|test-ok|test-failed|build-failed|build-failed|test-failed|skipped|skipped|test-failed|test-failed|skipped||
|bpm-19-3|Меремьянина Полина Сергеевна|meremyanina_p_s|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok||
|bpm-19-3|Миронов Иван Сергеевич|mironov_i_s||
|bpm-19-3|Мочалов Леонид Валерьевич|mochalov_l_v|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-3|Набиев Илкин Мехман оглы|nabiev_i_m|test-ok|build-failed|test-ok|skipped|skipped|skipped|skipped|test-ok|test-failed|skipped||
|bpm-19-3|Панкратов Антон Романович|pakratov_a_r|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-3|Савольский Лев Алексеевич|savolsky_l_a|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-failed|skipped|skipped||
|bpm-19-3|Семенов Егор Анатольевич|semenov_e_a||
|bpm-19-3|Тарашкевич Евгения Олеговна|tarashkevich_e_o||
|bpm-19-3|Тхор Игорь Николаевич|tkhor_i_n|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-3|Устинов Арсений Михайлович|ustinov_a_m|build-failed|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-3|Цао Чжэнь-Хао (Женя)|tcao_c|test-ok|build-failed|build-failed|build-failed|build-failed|skipped|skipped|skipped|build-failed|skipped||
|bpm-19-3|Чулков Никита Константинович|chulkov_n_k||
|bpm-19-3|Шапкова Елизавета Сергеевна|shapkova_e_s|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-3|Сидоров Никита Николаевич|sidorov_n_n||

|group|student|user|complex|rational|matrixds|matrixls|matrixvs|matrixdr|matrixlr|matrixvr|stackl|queuea|notes|
|-----|-------|----|------|------|------|------|------|------|------|------|------|------|-----|
|bpm-19-4|Алуф Давид|aluf_d||
|bpm-19-4|Блинов Дмитрий|blinov_d_s||
|bpm-19-4|Буйволов Евгений|buivolov_e_a|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-4|Вологин Иван|vologin_i||
|bpm-19-4|Гузняков Иван|guznyakov_i||
|bpm-19-4|Емельященкова Екатерина|emelyashchenkova_e|build-failed|skipped|build-failed|build-failed|build-failed|build-failed|skipped|skipped|build-failed|build-failed||
|bpm-19-4|Епанечников Юрий|epanechnikov_y||
|bpm-19-4|Еременко Сергей|eremenko_s|build-failed|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-4|Ерофеев Владислав|erofeev_v||
|bpm-19-4|Зейналов Эмиль|zejnalov_e||
|bpm-19-4|Иванов Никита Сергеевич|ivanov_n_s|build-failed|build-failed|skipped|skipped|build-failed|skipped|skipped|skipped|skipped|skipped||
|bpm-19-4|Ищенко Дмитрий|ishchenko_d|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-4|Кожан Дарья|kozhan_d_v|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-4|Корбутова Дарья|korbutova_d|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-4|Костромин Александр|kostromin_a|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-4|Левичкин Марк|levichkin_m|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-4|Левчук Иван|levchuk_i||
|bpm-19-4|Мельникова Мария|melnikova_m||
|bpm-19-4|Мефедов Антоний|mefedov_a||
|bpm-19-4|Павленко Владислав|pavlenko_v||
|bpm-19-4|Петросян Карен|petrosian_k_||
|bpm-19-4|Ратке Михаил|ratke_m||
|bpm-19-4|Сосенко Ярослав|sosenko_y||
|bpm-19-4|Хонер Павел|honer_p|build-failed|build-failed|build-failed|build-failed|build-failed|skipped|skipped|skipped|skipped|skipped||
|bpm-19-4|Хоршикян Гурген|horshikyan_g||
|bpm-19-4|Чистов Андрей|chistov_a||
|bpm-19-4|Чубенко Никита|chubenko_n||
|bpm-19-4|Якубов Илья|yakubov_i||

### 25.12.2020

|group|student|user|complex|rational|matrixds|matrixls|matrixvs|matrixdr|matrixlr|matrixvr|stackl|queuea|notes|
|-----|-------|----|------|------|------|------|------|------|------|------|------|------|-----|
|bpm-19-1|Абдуллин Амир|abdullin_a_a|build-failed|build-failed|skipped|skipped|build-failed|skipped|skipped|build-failed|skipped|skipped||
|bpm-19-1|Альмиева Ралина|almieva_r_r|test-ok|test-failed|test-ok|test-ok|test-ok|test-ok|test-failed|test-ok|skipped|skipped||
|bpm-19-1|Балаев Антон|balaev_a_a|build-failed|build-failed|skipped|skipped|build-failed|skipped|skipped|skipped|skipped|skipped||
|bpm-19-1|Бекетов Роман|beketov_r_i|build-failed|build-failed|skipped|skipped|build-failed|skipped|skipped|skipped|skipped|skipped||
|bpm-19-1|Воеводина Виктория|voevodina_v_e|test-ok|test-failed|build-failed|build-failed|build-failed|build-failed|skipped|skipped|skipped|skipped||
|bpm-19-1|Волков Сергей|volkov_s||
|bpm-19-1|Дин Дмитрий|din_d||
|bpm-19-1|Дьяков Владислав|dyakov_v_d|build-failed|test-ok|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-1|Ефремов Даниил|efremov_d_m|build-failed|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-1|Козина Анастасия|kozina_a_e|test-ok|test-failed|test-ok|test-ok|test-ok|skipped|skipped|build-failed|skipped|skipped||
|bpm-19-1|Колесникова Дарья|kolesnikova_d_p|build-failed|build-failed|skipped|skipped|build-failed|skipped|skipped|skipped|skipped|skipped||
|bpm-19-1|Кувшинов Марк|kuvshinov_m_e|build-failed|build-failed|build-failed|build-failed|build-failed|skipped|skipped|skipped|skipped|skipped||
|bpm-19-1|Личко Дмитрий|lichko_d_a|test-failed|test-failed|test-ok|test-ok|test-ok|test-ok|test-failed|test-failed|skipped|skipped||
|bpm-19-1|Марданшин Камиль|mardanshin_k_i|test-ok|test-failed|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-failed|test-failed||
|bpm-19-1|Матяш Дмитрий|matyash_d_s|build-failed|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-1|Мисютин Вячеслав|misutin_v_a|build-failed|test-failed|test-ok|skipped|test-ok|skipped|skipped|skipped|skipped|skipped||
|bpm-19-1|Мырзаханов Михаил|myrzakhanov_m_a|test-ok|test-failed|build-failed|build-failed|test-failed|build-failed|build-failed|build-failed|skipped|skipped||
|bpm-19-1|Пискевич Дариуш|piskevich_d||
|bpm-19-1|Полетаев Дмитрий|poletaev_d_i|test-ok|test-failed|skipped|skipped|build-failed|skipped|skipped|skipped|skipped|skipped||
|bpm-19-1|Попов Дмитрий|popov_d||
|bpm-19-1|Пырко Алексей|pyrko_a_v|test-ok|build-failed|test-ok|test-ok|test-ok|skipped|skipped|skipped|skipped|skipped||
|bpm-19-1|Радыгин Никита|radygin_n_s|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-1|Рамзайцев Даниил|ramzaicev_d_a|test-ok|test-failed|test-failed|test-failed|test-failed|test-failed|test-failed|test-failed|skipped|skipped||
|bpm-19-1|Рукосуев Валерий|rukosuev_v||
|bpm-19-1|Сергеева Дарья|sergeeva_d_v|build-failed|build-failed|skipped|skipped|build-failed|skipped|skipped|skipped|skipped|skipped||
|bpm-19-1|Сидулин Данила|sidulin_d_a|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-1|Синицын Богдан|sinicyn_b||
|bpm-19-1|Тищенко Анастасия|tischenko_a|build-failed|test-ok|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-1|Федоров Илья|fedorov_i_a||
|bpm-19-1|Фоменко Елена|fomenko_e_e|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||

|group|student|user|complex|rational|matrixds|matrixls|matrixvs|matrixdr|matrixlr|matrixvr|stackl|queuea|notes|
|-----|-------|----|------|------|------|------|------|------|------|------|------|------|-----|
|bpm-19-2|Акимова Алина|akimova_a_a|build-failed|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-2|Антонов Илья|antonov_i_a|build-failed|build-failed|build-failed|build-failed|build-failed|build-failed|skipped|skipped|test-failed|test-failed||
|bpm-19-2|Багильдинская Мария|bagildinskaia_m_s|build-failed|build-failed|skipped|skipped|build-failed|skipped|skipped|skipped|skipped|skipped||
|bpm-19-2|Богатов Дмитрий|bogatov_d_s||
|bpm-19-2|Богданов Артём|bogdanov_a_a|test-ok|test-failed|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-failed|test-failed||
|bpm-19-2|Богомолов Владислав|bogomolov_v|build-failed|build-failed|build-failed|skipped|build-failed|skipped|skipped|skipped|skipped|skipped||
|bpm-19-2|Борисова Софья|borisova_s_r|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-2|Гаврилова Анастасия|gavrilova_a_y|build-failed|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-2|Григорьева Алина|grigoryeva_a_r|build-failed|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-2|Дубинский Даниил|dubinsky_d_d|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-2|Епифанов Кирилл|epifanov_k_a|test-ok|test-failed|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-failed|test-failed||
|bpm-19-2|Ивершин Вадим|ivershin_v_s|test-ok|test-failed|test-ok|test-ok|test-ok|test-failed|test-ok|test-failed|test-failed|build-failed||
|bpm-19-2|Исаченко Михаил|isachenko_m_k|test-ok|test-failed|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-failed|test-failed||
|bpm-19-2|Ишмухамедов Артём|ishmuhamedov_a||
|bpm-19-2|Карнаушко Владимир|karnaushko_V|test-ok|test-failed|test-ok|test-ok|test-failed|test-ok|test-failed|test-ok|test-failed|test-failed||
|bpm-19-2|Комлев Данила|komlev_d_a|test-ok|build-failed|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-failed|test-ok||
|bpm-19-2|Котелевский Артем|kotelevsky_a_a|test-ok|build-failed|build-failed|skipped|build-failed|skipped|skipped|skipped|skipped|skipped||
|bpm-19-2|Круглов Артём|kruglov_a_m||
|bpm-19-2|Мелёхин Никита|melehin_n_i|build-failed|build-failed|build-failed|skipped|build-failed|skipped|skipped|skipped|skipped|skipped||
|bpm-19-2|Панин Григорий|panin_g_i|build-failed|build-failed|test-failed|test-failed|test-failed|test-failed|build-failed|test-failed|skipped|skipped||
|bpm-19-2|Парчиев Рауф|parchiev_r_b|test-ok|test-failed|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-failed|test-failed||
|bpm-19-2|Пачков Вячеслав|pachkov_v_v||
|bpm-19-2|Погонин Сергей|pogonin_s_a|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-2|Ханевский Артем|khanevsky_a_g||
|bpm-19-2|Цинпаев Умар|tcinpaev_u_s|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-2|Черных Александр|chernykh_a||
|bpm-19-2|Чуприна Мария|chuprina_m_s|test-ok|build-failed|test-ok|test-ok|test-ok|test-failed|test-ok|test-ok|test-failed|test-failed||

|group|student|user|complex|rational|matrixds|matrixls|matrixvs|matrixdr|matrixlr|matrixvr|stackl|queuea|notes|
|-----|-------|----|------|------|------|------|------|------|------|------|------|------|-----|
|bpm-19-3|Абрамов Денис Кириллович|abramov_d_k|build-failed|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-3|Астраханцев Никита Павлович|astrahancev_n_p||
|bpm-19-3|Гаврин Илья Антонович|gavrin_i_a|build-failed|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-3|Данкин Данила Владимирович|dankin_d_v||
|bpm-19-3|Золотухин Федор Владимирович|zolotuhin_f_v||
|bpm-19-3|Измайлов Лев Сергеевич|izmaylov_l_s|build-failed|build-failed|build-failed|skipped|build-failed|skipped|skipped|skipped|skipped|skipped||
|bpm-19-3|Камилов Айдунбек Саидов|kamilov_a_s||
|bpm-19-3|Ким Александр Станиславович|kim_a_s||
|bpm-19-3|Клейменов Владимир Евгеньевич|kleymenov_v_e|test-failed|test-failed|skipped|skipped|build-failed|skipped|skipped|build-failed|skipped|skipped||
|bpm-19-3|Кокаев Глеб Константинович|kokaev_g_k||
|bpm-19-3|Конев Тимофей Владимирович|konev_t_v|build-failed|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-3|Кузнецов Денис Андреевич|kuznetsov_d_a|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-3|Курлов Игорь Алексеевич|kurlov_i_a|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-3|Максаков Владимир Александрович|maksakov_v_a|test-failed|test-failed|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-3|Машуров Владимир Владимирович|mashurov_v_v|test-ok|test-failed|build-failed|build-failed|test-failed|skipped|skipped|skipped|test-failed|skipped||
|bpm-19-3|Меремьянина Полина Сергеевна|meremyanina_p_s|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok|test-ok||
|bpm-19-3|Миронов Иван Сергеевич|mironov_i_s||
|bpm-19-3|Мочалов Леонид Валерьевич|mochalov_l_v|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-3|Набиев Илкин Мехман оглы|nabiev_i_m|test-ok|build-failed|test-ok|skipped|skipped|skipped|skipped|test-ok|skipped|skipped||
|bpm-19-3|Панкратов Антон Романович|pakratov_a_r|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-3|Савольский Лев Алексеевич|savolsky_l_a|test-ok|test-failed|test-ok|test-ok|test-ok|skipped|skipped|test-failed|skipped|skipped||
|bpm-19-3|Семенов Егор Анатольевич|semenov_e_a||
|bpm-19-3|Тарашкевич Евгения Олеговна|tarashkevich_e_o||
|bpm-19-3|Тхор Игорь Николаевич|tkhor_i_n|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-3|Устинов Арсений Михайлович|ustinov_a_m|build-failed|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-3|Цао Чжэнь-Хао (Женя)|tcao_c|test-ok|build-failed|build-failed|build-failed|build-failed|skipped|skipped|skipped|build-failed|skipped||
|bpm-19-3|Чулков Никита Константинович|chulkov_n_k||
|bpm-19-3|Шапкова Елизавета Сергеевна|shapkova_e_s|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-3|Сидоров Никита Николаевич|sidorov_n_n||

|group|student|user|complex|rational|matrixds|matrixls|matrixvs|matrixdr|matrixlr|matrixvr|stackl|queuea|notes|
|-----|-------|----|------|------|------|------|------|------|------|------|------|------|-----|
|bpm-19-4|Алуф Давид|aluf_d||
|bpm-19-4|Блинов Дмитрий|blinov_d_s||
|bpm-19-4|Буйволов Евгений|buivolov_e_a|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-4|Вологин Иван|vologin_i||
|bpm-19-4|Гузняков Иван|guznyakov_i||
|bpm-19-4|Емельященкова Екатерина|emelyashchenkova_e|build-failed|skipped|build-failed|build-failed|build-failed|build-failed|skipped|skipped|build-failed|build-failed||
|bpm-19-4|Епанечников Юрий|epanechnikov_y||
|bpm-19-4|Еременко Сергей|eremenko_s|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-4|Ерофеев Владислав|erofeev_v||
|bpm-19-4|Зейналов Эмиль|zejnalov_e||
|bpm-19-4|Иванов Никита Сергеевич|ivanov_n_s|build-failed|build-failed|skipped|skipped|build-failed|skipped|skipped|skipped|skipped|skipped||
|bpm-19-4|Ищенко Дмитрий|ishchenko_d|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-4|Кожан Дарья|kozhan_d_v|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-4|Корбутова Дарья|korbutova_d|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-4|Костромин Александр|kostromin_a|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-4|Левичкин Марк|levichkin_m|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-4|Левчук Иван|levchuk_i||
|bpm-19-4|Мельникова Мария|melnikova_m||
|bpm-19-4|Мефедов Антоний|mefedov_a||
|bpm-19-4|Павленко Владислав|pavlenko_v||
|bpm-19-4|Петросян Карен|petrosian_k_||
|bpm-19-4|Ратке Михаил|ratke_m||
|bpm-19-4|Сосенко Ярослав|sosenko_y||
|bpm-19-4|Хонер Павел|honer_p|build-failed|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped|skipped||
|bpm-19-4|Хоршикян Гурген|horshikyan_g||
|bpm-19-4|Чистов Андрей|chistov_a||
|bpm-19-4|Чубенко Никита|chubenko_n||
|bpm-19-4|Якубов Илья|yakubov_i||
